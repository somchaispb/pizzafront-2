module.exports = {
    configureWebpack: config => {
        if (process.env.NODE_ENV === 'production') {

        } else {
            // изменения для разработки...
        }
    },
  "lintOnSave": false,
  "transpileDependencies": [
    "vuetify"
  ],
    devServer: {
        proxy: {
            '/api': {
                target: 'http://localhost:8080',
                ws: true,
                changeOrigin: true,
                pathRewrite: {
                    '^/api': ''
                }
            }
        }
    },

}