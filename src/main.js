import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import axios from 'axios';
import VueAxios from 'vue-axios';
import VueNoty from 'vuejs-noty'
import 'vuejs-noty/dist/vuejs-noty.css'
import VueSwal from 'vue-swal'
import AXIOS from "./router/axios";

let token = store.getters.getAccessToken;

AXIOS.interceptors.request.use((config) => {
    if (token){
        config.headers['Authorization'] = 'Bearer ' + token
    }
    config.headers['Content-type'] = "application/json"

    // Do something before request is sent
    return config;
}, function (error) {
    // Do something with request error
    return Promise.reject(error);
});

// Add a response interceptor
AXIOS.interceptors.response.use(function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response;
}, function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    if (401 === error.response.status) {
        swal({
            title: "Restricted",
            text: "You are not allowed to be here, do you want to redirect to main page?",
            icon: "warning",
            // buttons: true,
            button: "Yes",
            closeModal: false
        }).then(() => {
            window.location = '/';
        });
    } else {
        return Promise.reject(error);
    }
});

Vue.config.productionTip = false

Vue.use(VueSwal)
Vue.use(VueAxios, axios)
Vue.use(VueNoty, {
  timeout: 2000,
  theme: 'relax',
  layout: 'bottomLeft',
  progressBar: true
})


new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
