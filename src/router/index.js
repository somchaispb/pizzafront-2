import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import Register from '../views/Register'
import Orders from '../views/Orders'
import Checkout from '../views/Checkout'
import Manager from '../views/Manager'
import AddItem from '../views/AddItem'
import EnableItems from '../views/ChangeItemsEnabledStatus'
import Cart from '../components/Cart.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/register',
    name: 'register',
    component: Register
  },
  {
    path: '/orders',
    name: 'orders',
    component: Orders
  },
  {
    path: '/manager',
    name: 'manager',
    component: Manager
  },
  {
    path: '/checkout',
    name: 'checkout',
    component: Checkout
  },
  {
    path: '/addItem',
    name: 'addItem',
    component: AddItem
  },
  {
    path: '/enableItems',
    name: 'enableItems',
    component: EnableItems
  },
  {
    path: '/cart',
    name: 'cart',
    component: Cart
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
