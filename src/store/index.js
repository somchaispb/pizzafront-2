import Vue from 'vue'
import Vuex from 'vuex'
import AXIOS from '../router/axios';

Vue.use(Vuex)


export default new Vuex.Store({
  state: {
      menu: [],
      cart: JSON.parse(localStorage.getItem('cart')) || [],
      role: JSON.parse(localStorage.getItem('roles')) || [],
      accessToken: localStorage.getItem('accessToken') || '',
      user:  JSON.parse(localStorage.getItem('userProfile')) || '',
      orders: [],
      statuses: [],
      status: '',
  },
  mutations: {
      ADD_TO_CART(state, payload) {
          state.cart.push(payload)
          localStorage.setItem('cart', JSON.stringify(state.cart))
      },
      CLEAR_CART(state) {
          localStorage.removeItem('cart')
          state.cart = [];
      },
      SET_ACCESS_TOKEN(state, payload) {
          localStorage.setItem('accessToken', payload);
          state.status = 'success';
          state.accessToken = payload
      },
      CLEAR_ACCESS_TOKEN() {
          localStorage.removeItem('accessToken')
      },
      SET_USER_ROLE(state, payload) {
          payload.forEach(function (role) {
              state.role.push(role.name)
          })
          localStorage.setItem('roles', JSON.stringify(state.role))
      },
      SET_USER(state, payload) {
          localStorage.setItem('userProfile', JSON.stringify(payload))
          state.user = payload
      },
      CLEAR_USER(state) {
          localStorage.removeItem('userProfile')
          state.user = []
      },
      CLEAR_USER_ROLE(state) {
          localStorage.removeItem('roles')
          state.role = []
      },
      GET_USER_ORDERS(state, payload) {
          state.orders = payload
      },
      GET_ALL_ORDERS(state, payload) {
          state.orders = payload
      },
      GET_ADMIN_ORDERS(state, payload) {
          state.Adminrders = payload
      },
      GET_STATUSES(state, payload) {
          state.statuses = payload
      }

  },
  actions: {
      getUser({commit}) {
          AXIOS.post('/api/getUser', '').then(res => {
              commit('SET_USER', res.data.user)
          }).catch(err => console.log(err.message))
      },
      getUserOrders({commit}) {
          AXIOS.get('/api/orders').then(res => {
              commit('GET_USER_ORDERS', res.data)
          }).catch(err => console.log(err.message))
      },
      getAllOrders({commit}) {
          AXIOS.get('/api/admin/orders').then(res => {
              commit('GET_ALL_ORDERS', res.data)
          }).catch(err => console.log(err.message))
      },
      getAdminMenu({commit}) {
          AXIOS.get('/api/admin/menu').then(res => {
              commit('GET_ALL_ORDERS', res.data)
          }).catch(err => console.log(err.message))
      },
      getAllStatuses({commit}) {
          AXIOS.get('/api/statuses').then(res => {
              commit('GET_STATUSES', res.data)
          }).catch(err => console.log(err.message))
      },
  },
    getters: {
        getAccessToken: state => {
            return state.accessToken
        },
        isAuthenticated: state => !!state.accessToken,
        authStatus: state => state.status,
    },
  modules: {

  }
})
