# Pizza App Front

[This app is a part of backend application](https://bitbucket.org/somchaispb/pizzaappback
)
## Project setup
```
npm install
```



### Fast start without nginx
Following front app is requesting back API URL through proxy. Base API URL is "/api" without URI rewrite mode. If you run in develop mode, all API proxies are already configured
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```
If you build front app for production, you need to set up proxy API URL.
Example config for NGINX you can find below
```
server{

    server_name localhost;
    listen 80;

    access_log /var/log/nginx/mysite.access.log;
    error_log  /var/log/nginx/mysite.error.log;
    
    root /var/www/html/pizzaAppFront/dist;

    location ~* \.(?:ico|css|js|gif|jpe?g|png)$ {
        # Some basic cache-control for static files to be sent to the browser
        expires max;
        add_header Pragma public;
        add_header Cache-Control "public, must-revalidate, proxy-revalidate";
    }
    
    location /api {
        add_header X-Cache-Status $upstream_cache_status;
        proxy_next_upstream     error timeout invalid_header http_500;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        #proxy_set_header 'Access-Control-Allow-Origin' $http_origin;
        
        # Here you should put URL of Backend APP, for example you have backend hosted on localhost:8080
        proxy_pass http://127.0.0.1:8080;
        
        proxy_redirect     off;
    }
    
    location /
    {
        try_files $uri $uri/ /index.html;
    }
    
    location /errors {
         alias /home/www-data/mysite/errors ;
    }
    error_page 404 /errors/404.html;        
}

```
